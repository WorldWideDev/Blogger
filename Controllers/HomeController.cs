﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using BlogSite.Models;
using Microsoft.EntityFrameworkCore;

namespace BlogSite.Controllers
{
    public class BlogController : Controller
    {
        private BlogContext _dbContext;
        public BlogController(BlogContext context)
        {
            _dbContext = context;
        }
        [HttpGet("")]
        public IActionResult Index()
        {
            // we need a list of blogs!
            return View(_dbContext.Blogs
                .Include(b => b.Creator)
                .ToList());
        }
        [HttpGet("{id}")]
        public IActionResult Show(int id)
        {
            // we need a blog!
            return View(_dbContext.Blogs
                .Include(b => b.Creator)
                .ThenInclude(u => u.CreatedBlogs)
                .SingleOrDefault(b => b.BlogId == id));
        }
      
    }
}
