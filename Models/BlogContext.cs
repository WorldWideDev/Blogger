using Microsoft.EntityFrameworkCore;
using BlogSite.Models;

namespace BlogSite
{
    public class BlogContext : DbContext
    {
        public BlogContext(DbContextOptions options) : base(options) {}
        public DbSet<User> Users {get;set;}
        public DbSet<Blog> Blogs {get;set;}
    }

}