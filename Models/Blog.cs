using System;
using System.ComponentModel.DataAnnotations;

namespace BlogSite.Models
{
    public class Blog
    {
        [Key]
        public int BlogId {get;set;}
        public string Title {get;set;}
        public string Content {get;set;}
        public int Intensity {get;set;}
        public DateTime CreatedAt {get;set;}
        public int UserId {get;set;}
        // ^^^ IN DB

        // vvv NOT IN DB: FOR EF
        public User Creator {get;set;}
    }
}